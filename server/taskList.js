var fs 						= require('fs');
var querystring 	= require("querystring");

var taskList = null;
var taskListDataString;
var sessions 	= {};

fs.readFileSync('db/taskList.json', 'utf8' ,function(err, data){
	if(err){
		console.log('error reading file: ' + err);
		return;
	}
	taskListDataString = data;
});
function buildResponse(response,tasksObject){
	response.writeHead(200, {
		"Content-Type": "text/plain"
	});
	response.write(JSON.stringify(tasksObject));	
	response.end();
	return response;
}
function readAllTasksByUserId(userId){
	dataFromFile = fs.readFileSync("db/taskList.json", 'utf8');
	var tasksObject = Array();
	var userTasks = Array();
	try {
		var tasksObject = JSON.parse(dataFromFile);
		console.log("allTasks:" + tasksObject);
		return findTasksByUserId(userId,tasksObject);
	}
	catch(err) {
		console.log("error:" + err);
	}
	return userTasks;
}

function pullTasks(response, parsedUrl, postData){
	var parsedQuery = querystring.parse(postData);
	var userId = parsedQuery.userId;
	console.log("pullTasks userId=" + userId); 
	var tasksUser = readAllTasksByUserId(userId);
	
	buildResponse(response,tasksUser);
}
function pushTask(response, parsedUrl, postData){
	var parsedQuery = querystring.parse(postData);
	dataFromFile = fs.readFileSync("db/taskList.json", 'utf8');
	var tasksObject = Array();
	try {
		var tasksObject = JSON.parse(dataFromFile);
		console.log("userId:" + parsedQuery.userId);
		
	}
	catch(err) {
		console.log("error:" + err);
	}
	var task = findTask(tasksObject,parsedQuery.id);
	if(task != null){
		task.name = parsedQuery.name;
	}
	else{
		tasksObject[tasksObject.length] = {"userId": parsedQuery.userId,"id": parsedQuery.id, "name": parsedQuery.name};
	}
	fs.writeFile('db/taskList.json', JSON.stringify(tasksObject), 'utf8');
	buildResponse(response,findTasksByUserId(parsedQuery.userId,tasksObject));
	return true;
}
function deleteTask(response, parsedUrl, postData){
	var parsedQuery = querystring.parse(postData);
	dataFromFile = fs.readFileSync("db/taskList.json", 'utf8');
	var tasksObject = Array();
	try {
		var tasksObject = JSON.parse(dataFromFile);
		
	}
	catch(err) {
		console.log("error:" + err);
	}
	var task = findTask(tasksObject,parsedQuery.id);
	if(task != null){
		tasksObject.splice(parsedQuery.id,1);
	}
	fs.writeFile('db/taskList.json', JSON.stringify(tasksObject), 'utf8');
	buildResponse(response,findTasksByUserId(task.userId,tasksObject));
	
	return true;
}
function pushSubTask(response, parsedUrl, postData){
	var parsedQuery = querystring.parse(postData);
	dataFromFile = fs.readFileSync("db/taskList.json", 'utf8');
	var tasksObject = Array();
	try {
		var tasksObject = JSON.parse(dataFromFile);
		var subTask = findSubTask(tasksObject,parsedQuery.taskId,parsedQuery.id);
		if(subTask != null){
			subTask.name = parsedQuery.name;
			subTask.isChecked = parsedQuery.isChecked;
		}
		else{
			var subTask = {name:parsedQuery.name, id:parsedQuery.id, isChecked:parsedQuery.isChecked};
			if(task.subTasksArray == null){
				task.subTasksArray = new Array();
			}
			task.subTasksArray.push(subTask);
		}
	}
	catch(err) {
		console.log("error:" + err);
	}
	fs.writeFile('db/taskList.json', JSON.stringify(tasksObject), 'utf8');
	buildResponse(response,findTasksByUserId(task.userId,tasksObject));
	return true;
}
function deleteSubTask(response, parsedUrl, postData){
	var parsedQuery = querystring.parse(postData);
	dataFromFile = fs.readFileSync("db/taskList.json", 'utf8');
	var tasksObject = Array();
	try {
		var tasksObject = JSON.parse(dataFromFile);
		var task = findTask(tasksObject,parsedQuery.taskId);
		console.log("taskId:" + task.id);
		if(task != null){
			if(task.subTasksArray == null){
				return;
			}
			var subTaskId =  parsedQuery.subTaskId;
			task.subTasksArray.splice(subTaskId,1);
		}
	}
	catch(err) {
		console.log("error:" + err);
	}
	fs.writeFile('db/taskList.json', JSON.stringify(tasksObject), 'utf8');
	buildResponse(response,findTasksByUserId(task.userId,tasksObject));
	return true;
}
function findTasksByUserId(userId,tasksObject){
	var userTasks = Array();
	for(var i=0;i<tasksObject.length;i++){
		var task = tasksObject[i];
		if(task.userId == userId){
			userTasks.push(task);
		}
	}
	
	return userTasks;
}
function findTask(tasksObject,taskId){
	for(var i=0;i<tasksObject.length;i++){
		task = tasksObject[i];
		if(task.id == taskId){
			return task;
		}
	}
	return null;
}
function findSubTask(tasksObject,taskId,subTaskId){
	var task = findTask(tasksObject,taskId);
	if(task != null){
		if(task.subTasksArray == null){
			return null;
		}
		for(var i=0;i<task.subTasksArray.length;i++){
		var subTask = task.subTasksArray[i];
		if(subTask.id == subTaskId){
			return subTask;
		}
	}
	}
	return null;
}
exports.pullTasks 	  = pullTasks,
exports.pushTask 	  = pushTask,
exports.deleteTask     = deleteTask,
exports.pushSubTask   = pushSubTask,
exports.deleteSubTask = deleteSubTask;