var querystring = require('querystring');
var db					= require('./db');
var auth				= require('./auth');
var fs 						= require('fs');


function upload(response, pathname, postData) {
  console.log("upload was called");
  response.writeHead(200, {"Content-Type": "text/plain"});
  var parsedData = querystring.parse(postData).text;
  response.write("You have sent: " + parsedData);
  response.end();
}

function validateCreateUserParams (parsedQuery) {
  return (typeof parsedQuery.user === 'string' &&
  typeof parsedQuery.password === 'string' &&
  parsedQuery.user !== "" && parsedQuery.password !== "");
}

function createUser (response, parsedUrl, postData){
  var parsedQuery = querystring.parse(postData);
  if (validateCreateUserParams(parsedQuery)){
    if (auth.createUser(parsedQuery.user, parsedQuery.password)){
      db.createUser(parsedQuery.user, parsedQuery.properties);
      response.writeHead(200, {"Content-Type": "text/plain"});
      response.write("we are creating user: " + parsedQuery.user);
      response.end();
      return;
    }
  }
  response.writeHead(500, {"Content-Type": "text/plain"});
  response.write("fail to create new user");
  response.end();
}
function saveScreen (response, parsedUrl, postData){
	var parsedQuery = querystring.parse(postData);
	var leftCenterWidth = parsedQuery.leftCenterWidth;
	var leftCenterHeight = parsedQuery.leftCenterHeight;
	var userId = parsedQuery.userId;
	
	dataFromFile = fs.readFileSync("db/preferenceScreen.json", 'utf8');
	var preferences = Array();
	try {
		var preferences = JSON.parse(dataFromFile);
		var preference = findPreferenceByUserId(preferences,userId);
		if(preference != null){
			preference.leftCenterWidth = leftCenterWidth;
			preference.leftCenterHeight = leftCenterHeight;
		}
		else{
			preferences[preferences.length] = {"userId": userId,"leftCenterWidth": leftCenterWidth, "leftCenterHeight": leftCenterHeight};
		}

	}
	catch(err) {
		console.log("error:" + err);
	}
	fs.writeFile('db/preferenceScreen.json', JSON.stringify(preferences), 'utf8');
	
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.end();
}
function findPreferenceByUserId(preferences,userId){
	for(var i=0;i<preferences.length;i++){
		var preference = preferences[i];
		if(preference.userId == userId){
			return preference;
		}
	}
	return null;
}
function pullPreferenceByUserId(response, parsedUrl, postData){
	var parsedQuery = querystring.parse(postData);
	var userId = parsedQuery.userId;
	console.log("userId:" + userId);
	var preference = readAllPreferencesByUserId(userId);
	console.log("preference:" + preference);
	response.writeHead(200, {
		"Content-Type": "text/plain"
	});
	response.write(JSON.stringify(preference));	
	response.end();
}
function readAllPreferencesByUserId(userId){
	dataFromFile = fs.readFileSync("db/preferenceScreen.json", 'utf8');
	var preferencesScreen = Array();
	var preference = Array();
	try {
		var preferencesScreen = JSON.parse(dataFromFile);
		return findPreferenceByUserId(preferencesScreen,userId);
	}
	catch(err) {
		console.log("error:" + err);
	}
	return preference;
}

exports.upload      = upload;
exports.createUser  = createUser;
exports.saveScreen  = saveScreen;
exports.pullPreferenceByUserId = pullPreferenceByUserId;