# SuperList
Single page application, with node.js server, educational project

30/08/2015 נושאים מתקדמים בתיכנות-פרוייקט סוף

Hofit Tamam & Stav Naveh Halali

SuperList: is a single page application in witch the user can create his own lists and sub lists,
the user can add description, edit, delete and mark the lists as "done".

This application is written in Java script using Jquery,HTML,Kendo-ui library (spliters,buttons,tabs and popups).

In order to run SuperList make sure you have downloaded nodejs. dounloade the project from: and go to the folder "server" and open cmd on that location. then write: node main.js , hit enter.
go to chrome browser and at the URL adress write: localhost:8888 (by default). logIn user name:moshe password:12345678.
now you can start working.

**This project is not for production uses**

## Installation
From the project directory:
```bash
#client installation
cd client
npm install -g bower
bower install
#server installation
cd ../server
npm install
```
## Running
```bash
cd server
node main.js
```
You can view the application at http://127.0.0.1:8888

**Do not use "localhost"** it makes some problems for cookies