
define(['jquery', 'kendo'], 
	function($){
		'use strict';
	
	function createData(){
		window.tasksListDataArr = new Array();
		pullTasksFromServer();
	}
	function pullTasksFromServer(){
		function onSuccessFunc(data){
			updateData(data);
		}

		function onFailureFunc(){
		 
		}
		request.pullTasks(window.userId,onSuccessFunc,onFailureFunc);
	}
	function saveTaskList(taskListName){
		var subTasks = new Array();
		var taskListObject = {userId:window.userId,name:taskListName, id:tasksListDataArr.length,subTasksArray:subTasks};
		pushTaskToServer(taskListObject);
		return taskListObject;
	}
	function editTaskList(taskListObject){
		pushTaskToServer(taskListObject);
		return taskListObject;
	}
	function deleteTaskList(taskListObject){
		function onSuccessFunc(data){
			updateData(data);
		}

		function onFailureFunc(){
		 
		}
		request.deleteTask(taskListObject,onSuccessFunc,onFailureFunc);
	}
	function pushTaskToServer(taskListObject){
		function onSuccessFunc(data){
			updateData(data);
		}

		function onFailureFunc(){
		 
		}
		request.pushTask(taskListObject,onSuccessFunc,onFailureFunc);
	}
	function updateData(data){
		var objs=[];
		var json = JSON.stringify(eval("(" + data + ")"));
		tasksListDataArr = JSON.parse(json);
		leftPanelLayout.reloadTaskListDataView();
	}
	function pushSubTaskToServer(taskId,subTask){
		function onSuccessFunc(data){
			updateData(data);
		}

		function onFailureFunc(){
		 
		}
		request.pushSubTask(taskId,subTask,onSuccessFunc,onFailureFunc);
	}
	function deleteSubTaskToServer(taskId,subTask){
		var task = tasksListDataArr[taskId];
		task.subTasksArray.splice(subTask.id,1);
		function onSuccessFunc(data){
		
		}

		function onFailureFunc(){
		 
		}
		request.deleteSubTask(taskId,subTask,onSuccessFunc,onFailureFunc);
	}
	return {
		createData: createData,
		saveTaskList: saveTaskList,
		editTaskList: editTaskList,
		deleteTaskList: deleteTaskList,
		pushSubTaskToServer: pushSubTaskToServer,
		deleteSubTaskToServer: deleteSubTaskToServer,
	}
});