

define(['jquery', 'kendo'], 
	function($){
		'use strict';
	
	function createAndAddDiv(containerId,id,className){
		var div = createDiv(id,className);
		document.getElementById(containerId).appendChild(div);
		return div;
	}
	function createDiv(id,className){
		return createElement('div',id,className);
	}
	function createButton(id,className){
		return createElement('button',id,className);
	}
	function createTextBox(id,className){
		return createElement('input',id,className);
	}
	function createAndAddTextBox(containerId,id,className){
		var textBox = createElement('input',id,className);
		document.getElementById(containerId).appendChild(textBox);
		return textBox;
	}
	function createElement(elementTypeName,id,className){
		var element = document.createElement(elementTypeName);
		element.id = id;
		element.className = className;
		return element;
	}
	function createAndAddElement(elementTypeName,containerId,id,className){
		var element = createElement(elementTypeName,id,className);
		document.getElementById(containerId).appendChild(element);
		return element;
	}
	function createAndAddCheckBox(containerId,id,className){
		var checkbox = createElement('input',id,className);
		checkbox.type = "checkbox";
		document.getElementById(containerId).appendChild(checkbox);
		return checkbox;
	}
	return {
		createAndAddDiv: createAndAddDiv,
		createDiv: createDiv,
		createButton: createButton,
		createTextBox: createTextBox,
		createAndAddTextBox: createAndAddTextBox,
		createElement: createElement,
		createAndAddElement: createAndAddElement,
		createAndAddCheckBox: createAndAddCheckBox
	}
});