define(['jquery', 'kendo'], 
	function($){
		'use strict';
	function openTaskPopup(taskListDataObject){
		window.taskListDataObject = taskListDataObject;
		window.isEditTask = taskListDataObject != null;
		var popName = 'createTaskListPopup';
		utilities.createAndAddDiv('mainScreen',popName,popName);
		$("#" + popName).kendoWindow({
			actions: [ 
				"Minimize", 
				"Maximize" 
			],
			position: {
				top: 100, 
				left: "50%",
				height: "50%",
				title: "Customer details"
			},
			title: "create task list"
		});
		addTitle(popName,isEditTask);
		addTaskTextBox(popName,taskListDataObject);
		addTaskSaveButton(popName);
		if(isEditTask){
			addDeleteTaskButton(popName);
		}
		addTaskCancelButton(popName);
		$("#" + popName).data("kendoWindow").open();
	}
	function addTitle(popName,isEditTask){
		var name = 'create task list'
		if(isEditTask){
			name = 'edit task list name'
		}
		var titleName = 'createTaskListPopupTitle';
		var title = utilities.createAndAddDiv(popName,titleName,titleName);
		$("#" + titleName).text = name;
		title.innerText = name;
	}
	function addTaskSaveButton(popName){
		var saveButtonName = 'addTaskListSaveButton';
		var saveButton = utilities.createAndAddDiv(popName,saveButtonName,saveButtonName);
		$("#" + saveButtonName).kendoButton({
			title: "save",
			click: function(e) {
				var textBox = document.getElementById('addTaskListTextBox');
				saveTaskList(textBox.value);
				closeAddTaskPopup(popName);
			}
		});
		saveButton.innerText = 'save';
	}
	function saveTaskList(taskListNameText){
		if(isEditTask){
			taskListDataObject.name = taskListNameText;
			data.editTaskList(taskListDataObject);
		}
		else{
			taskListDataObject = data.saveTaskList(taskListNameText);
			leftPanelLayout.addTaskListDataView(taskListDataObject);
		}
	}
	function addDeleteTaskButton(popName){
		var deleteButtonName = 'addTaskListDeleteButton';
		var deleteButton = utilities.createAndAddDiv(popName,deleteButtonName,deleteButtonName);
		$("#" + deleteButtonName).kendoButton({
			title: "delete",
			click: function(e) {
				data.deleteTaskList(taskListDataObject);
				closeAddTaskPopup(popName);
			}
		});
		deleteButton.innerText = 'delete';
	}
	function addTaskCancelButton(popName){
		var saveButtonName = 'addTaskListCancelButton';
		var saveButton = utilities.createAndAddDiv(popName,saveButtonName,saveButtonName);
		$("#" + saveButtonName).kendoButton({
			title: "cancel",
			click: function(e) {
				closeAddTaskPopup(popName);
			}
		});
		saveButton.innerText = 'cancel';
	}
	function addTaskTextBox(popName,taskListDataObject){
		var text = "";
		if(taskListDataObject != null){
			text = taskListDataObject.name;
		}
		var textBoxName = 'addTaskListTextBox';
		var textBox = utilities.createAndAddTextBox(popName,textBoxName,textBoxName);
		textBox.value = text;
	}
	function closeAddTaskPopup(popName){
		$("#" + popName).data('kendoWindow').close();
		var element = document.getElementById(popName);
		element.parentNode.removeChild(element);
	}

	return {
		openTaskPopup: openTaskPopup,
	}
});