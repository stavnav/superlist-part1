/*control the center pannel*/

define(['jquery', 'kendo'], 
	function($){
		'use strict';
		
	/*build the center panel in order to build we used utilities*/
	function buildCenterPanelLayout(centerPanelName){
		window.tabStripName = 'tabStripName';
		utilities.createAndAddElement('ul',centerPanelName,tabStripName,tabStripName);
		buildContentContainer();//after panel is build we build the content
	}
	
	function buildContentContainer(){
		var contentContainerName = 'contentContainer';
		var contentHeaderName = 'contentHeader';
		var contentName = 'content';
		utilities.createAndAddDiv(centerPanelName,contentContainerName,contentContainerName);
		var contentHeader = utilities.createAndAddDiv(contentContainerName,contentHeaderName,contentHeaderName);
		utilities.createAndAddDiv(contentContainerName,contentName,contentName);
		initContentHeader(contentHeaderName);
	}
	function initContentHeader(contentHeaderName){
		var textBoxName = 'subTaskName';
		utilities.createAndAddTextBox(contentHeaderName,textBoxName,textBoxName);
		
		var addSubTaskButtonName = 'addSubTaskButton';
		var addTaskButton = utilities.createButton(addSubTaskButtonName,addSubTaskButtonName);
		document.getElementById(contentHeaderName).appendChild(addTaskButton);
		$("#" + addSubTaskButtonName).kendoButton({
			imageUrl: "/images/addSubTaskImage.png",
			click: function(e) {
				onAddSubTaskClick();
			}
		});
	}
	
	/*this function is called when we want to add sub task*/
	function onAddSubTaskClick(){
		if(typeof selectedTaskId == 'undefined'){
			alert("you need to choose task");
			return;
		}
		var task = tasksListDataArr[selectedTaskId];
		var subTaskName = document.getElementById('subTaskName').value;
		if(task.subTasksArray == null){
			task.subTasksArray = new Array();//create array of sub task for each task
		}
		var subTask = {name:subTaskName, id:task.subTasksArray.length};
		data.pushSubTaskToServer(task.id,subTask);//push the data to the server
		onTabClick(task.id);//when a tab has clicked we send the function the task id
	}
	function addTabTaskList(taskListDataObject){
		var tabTaskListName = 'tabTaskListName';
		var id = tabTaskListName + taskListDataObject.id;
		var tabTaskList = document.getElementById(id);
		if(tabTaskList == null){//if there is no such tab then build it
			buildTab(taskListDataObject,tabTaskListName);
		}
		else{//else do click
			tabTaskList.innerText = taskListDataObject.name;
			tabTaskList.click();
		}
		
	}
	/*build new tab with data and name*/
	function buildTab(taskListDataObject,tabTaskListName){
		var id = tabTaskListName + taskListDataObject.id;
		var tabTaskList = utilities.createAndAddElement('li',tabStripName,id,tabTaskListName);
		tabTaskList.innerText = taskListDataObject.name;
		$("#" + id).kendoButton({
			click: function(e) {
				window.selectedTaskId = taskListDataObject.id;
				onTabClick(selectedTaskId);
			}
		});
	}
	
	/*function if called when a tab clicked it is build the view according to the selected tab*/
	function onTabClick(taskId){
		document.getElementById('subTaskName').value = "";
		var contentContainer = document.getElementById('content');
		contentContainer.innerHTML = '';
		var task = tasksListDataArr[taskId];
		var subTasks = task.subTasksArray;
		if(subTasks == null){
			return;
		}
		for(var i=0;i<subTasks.length;i++){
			var subTaskListRowName = 'subTaskListRowName' + i;
			var subTaskListRow = utilities.createAndAddDiv('content',subTaskListRowName,'taskListDataView');
			
			 $('#' + subTaskListRowName).click((function(id) {
				return function() {
					resetAllTaskListSelected();
					var element = document.getElementById(id);
					if(element != null){
						element.className = 'selectedTaskListDataView';
					}
				};
			}(subTaskListRowName)));
			addCheckBoxSubTask(subTaskListRowName,taskId,i);
			addTextSubTask(subTaskListRowName,subTasks[i]);
			addDeleteSubTaskButton(subTaskListRow,taskId,i);
			
		}
		
	}
	
	function addTextSubTask(subTaskListRowName,subTask){
		var textName = "textName";
		var div = utilities.createAndAddDiv(subTaskListRowName,textName + subTask.id,textName);
		div.innerText = subTask.name;
	}
	function addCheckBoxSubTask(subTaskListRowName,taskId,i){
		var checkBoxSubTaskName = 'checkBoxSubTaskName';
		var checkBoxSubTaskId = checkBoxSubTaskName + i;
		var checkBox = utilities.createAndAddCheckBox(subTaskListRowName,checkBoxSubTaskId,checkBoxSubTaskName);
		var subTask = tasksListDataArr[taskId].subTasksArray[i];
		var isChecked = false;
		if(subTask.isChecked == 'true'){
			isChecked = true;
		}
		checkBox.checked = isChecked;
		$('#' + checkBoxSubTaskId).click((function(taskId,i) {
			return function() {
				var subTask = tasksListDataArr[taskId].subTasksArray[i];
				subTask.isChecked = checkBox.checked;
				data.pushSubTaskToServer(taskId,subTask);
			};
		}(taskId,i)));
	}
	/*add the delete button next to the sub task*/
	function addDeleteSubTaskButton(subTaskListRow,taskId,i){
		var deleteSubTaskButtonName = 'deleteSubTaskButtonName' + i;
		var deleteSubTaskButton = utilities.createButton(deleteSubTaskButtonName,deleteSubTaskButtonName);
		deleteSubTaskButton.innerText = "delete";
		subTaskListRow.appendChild(deleteSubTaskButton);
		$('#' + deleteSubTaskButtonName).click((function(taskId,id) {
			return function() {
				data.deleteSubTaskToServer(taskId,id);
				onTabClick(taskId);
			};
		}(taskId,i)));
	}
	function resetAllTaskListSelected(){
		var contentName = 'content';
		var content = document.getElementById(contentName);
		for(var i=0;i<content.childNodes.length;i++){
			content.childNodes[i].className = 'taskListDataView';
		}
	}
	return {
		buildCenterPanelLayout: buildCenterPanelLayout,
		addTabTaskList: addTabTaskList
	}
});