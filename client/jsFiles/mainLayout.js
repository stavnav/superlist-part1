define(['jquery', 'kendo'], 
	function($){
		'use strict';
	
function buildSplitterLayout(){
	var mainPanelName = 'mainScreen';
	var mainScreen = utilities.createDiv(mainPanelName,mainPanelName);
	document.getElementsByTagName('body')[0].appendChild(mainScreen);
	buildMainSplitterLayout(mainPanelName);
}
function buildMainSplitterLayout(mainScreenName){
	window.isFirstTime = true;
	window.leftPanelName = 'leftPanel';
	window.centerPanelName = 'centerPanel';
	window.rightPanelName = 'rightPanel';
	utilities.createAndAddDiv(mainScreenName,leftPanelName,leftPanelName);
	utilities.createAndAddDiv(mainScreenName,centerPanelName,centerPanelName);
	utilities.createAndAddDiv(mainScreenName,rightPanelName,rightPanelName);
	leftPanelLayout.buildLeftPanelLayout(leftPanelName);
	centerPanelLayout.buildCenterPanelLayout(centerPanelName);
	buildSplitter(mainScreenName,leftPanelName);
	pullScreen();
	
}
/*this function takes care of the users preference -We were able to save the splitter's width and hight on the server
before log off and get it back from server when log in,
we were unable to set Splitter*/
function pullScreen(){
	function onSuccessFunc(data){
		var json = JSON.stringify(eval("(" + data + ")"));
		var preference = JSON.parse(json);
		var splitter=$("#leftPanel").data("kendoSplitter");
		splitter.size("#leftCenterPanel",preference.leftCenterWidth);
		//leftSplitter = $("#" + leftPanelName).data("kendoSplitter");
		//leftSplitter.size("#leftCenterSplitter",preference.leftCenterWidth);
		//$("#leftCenterPanel").css("width",preference.leftCenterWidth);
		//$("#leftCenterPanel").css("height",preference.leftCenterHeight);
		console.log("pullWidth:" + $("#leftCenterPanel").width());
	}

	function onFailureFunc(){
		 
	}
	request.pullScreen(window.userId,onSuccessFunc,onFailureFunc);
}
/*create the splitter*/
function buildSplitter(mainScreenName,leftPanelName){
	$("#" + mainScreenName).kendoSplitter({
                      
    });
	$(mainScreenName).kendoSplitter({
		orientation: "horizontal",
		panes: [
			{ collapsible: false, size: "20%" },
			{ collapsible: false, size: "60%" },
			{ collapsible: false, size: "20%" }
		],
        resize: onMainScreenResize
    });
    $("#" + leftPanelName).kendoSplitter({
        orientation: "vertical",
        panes: [
            { collapsible: false, size: "20%" },
            { collapsible: false, size: "60%" },
            { collapsible: false,resizable: false, size: "20%" }

        ],
        resize: onLeftPanelResize
    });
	function onLeftPanelResize(e) {
		if(window.userId == null || window.isFirstTime == true){
			window.isFirstTime = false;
			return;
		}
		var leftWidth =  $("#leftCenterPanel").width();
		var leftHeight = $("#leftCenterPanel").height();
		function onSuccessFunc(data){
			console.log("saveWidth:" + $("#leftCenterPanel").width());
		}

		function onFailureFunc(){
		 
		}
		request.saveScreen(window.userId,leftWidth,leftHeight,onSuccessFunc,onFailureFunc);
    }
	function onMainScreenResize(e) {
		//alert("rightPanelWidth:" + $("#rightPanel").width() + " rightPanelHeight:" + $("#rightPanel").height());
    }
}
	return {
		buildSplitterLayout: buildSplitterLayout,
	}
});