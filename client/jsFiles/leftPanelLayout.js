define(['jquery', 'kendo'], 
	function($){
		'use strict';
	/*build the left pannel*/
	function buildLeftPanelLayout(leftPanelName){
		var leftTopPanelName = 'leftTopPanel';
		var leftCenterPanelName = 'leftCenterPanel';
		var leftBottomPanelName = 'leftBottomPanel';
		var leftTopPanel = utilities.createAndAddDiv(leftPanelName,leftTopPanelName,leftTopPanelName);
		var leftCenterPanel = utilities.createAndAddDiv(leftPanelName,leftCenterPanelName,leftCenterPanelName);
		var leftBottomPanel = utilities.createAndAddDiv(leftPanelName,leftBottomPanelName,leftBottomPanelName);
		buildMenuItems(leftTopPanel);
		buildTaskListButton(leftBottomPanel);
	}
	/*build the menu at the top left pannel*/
	function buildMenuItems(panel){
		var menuItemsName = 'menuItems';
		var menuItems = utilities.createElement('ul',menuItemsName,menuItemsName);
		panel.appendChild(menuItems);
		
		var firstItemMenuName = 'firstItemMenu';
		var secondItemMenuName = 'secondItemMenu';
		var threeItemMenuName = 'threeItemMenu';

		var firstItem = utilities.createAndAddElement('li',menuItemsName,firstItemMenuName,firstItemMenuName);
		var secondItem = utilities.createAndAddElement('li',menuItemsName,secondItemMenuName,secondItemMenuName);
		var threeItem = utilities.createAndAddElement('li',menuItemsName,threeItemMenuName,threeItemMenuName);

		firstItem.innerText = 'item1';
		secondItem.innerText = 'item2';
		threeItem.innerText = 'item3';
		
		$("#" + menuItemsName).kendoMenu();
	}
	/*build the add task button */
	function buildTaskListButton(panel){
		var addTaskButtonName = 'addTaskButton';
		var addTaskButton = utilities.createButton(addTaskButtonName,addTaskButtonName);
		panel.appendChild(addTaskButton);
		$("#" + addTaskButtonName).kendoButton({
			imageUrl: "/images/addTaskImage.png",
			click: function(e) {
				popup.openTaskPopup(null);
			}
		});
	}
	/*reload data*/
	function reloadTaskListDataView(){
		var taskListName = 'leftCenterPanel';
		var taskList = document.getElementById(taskListName);
		taskList.innerHTML = '';
		for(var i=0;i<tasksListDataArr.length;i++){
			var taskListDataObject = tasksListDataArr[i];
			var taskListView = addTaskListDataView(taskListDataObject);
			if(window.selectedTaskListId != null && selectedTaskListId == taskListDataObject.id){
				onTaskListClick(taskListDataObject,taskListView);
			}
		}
	}
	function addTaskListDataView(taskListDataObject){
		var leftCenterPanel = 'leftCenterPanel';
		var taskListName = 'taskList' + taskListDataObject.id;
		
		var taskList = utilities.createAndAddDiv(leftCenterPanel,taskListName,taskListName);
		buildTaskListDataView(taskList,taskListDataObject);
		return taskList;

	}
	function buildTaskListDataView(taskList,taskListDataObject){
		var taskListRowName = 'taskListRowName';
		var taskListRow = utilities.createDiv(taskListRowName,taskListRowName);
		taskListRow.innerText = taskListDataObject.name;
		taskList.className = 'taskListDataView';
		taskList.onclick = function(){//when the user click list
			onTaskListClick(taskListDataObject,taskList);
		};
		taskList.appendChild(taskListRow);
		
		
		var editTaskButtonName = 'editTaskButtonName' + taskListDataObject.id;
		var editTaskButton = utilities.createButton(editTaskButtonName,editTaskButtonName);//create edit button
		editTaskButton.innerText = "edit";
		taskListRow.appendChild(editTaskButton);
		$('#' + editTaskButtonName).click((function(task) {//edit function for task
			return function() {
				popup.openTaskPopup(task);
			};
		}(taskListDataObject)));
	}
	function onTaskListClick(taskListDataObject,taskListView){
		window.selectedTaskListId = taskListDataObject.id;
		resetAllTaskListSelected();
		taskListView.className = 'selectedTaskListDataView';
		centerPanelLayout.addTabTaskList(taskListDataObject);
	}
	function resetAllTaskListSelected(){
		var leftCenterPanel = 'leftCenterPanel';
		var taskListList = document.getElementById(leftCenterPanel);
		for(var i=0;i<taskListList.childNodes.length;i++){
			taskListList.childNodes[i].className = 'taskListDataView';
		}
	}
	
	return {
		buildLeftPanelLayout: buildLeftPanelLayout,
		addTaskListDataView: addTaskListDataView,
		reloadTaskListDataView: reloadTaskListDataView,
	}
});