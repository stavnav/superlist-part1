define(['jquery'], function($){
	
	'use strict';

	function login(name, password){
		return $.ajax("/api/login", {
			method: "post",
			success: function(data, a, xhr){
				console.log(data)
				var userIdArray = data.split("userId=");
				window.userId = userIdArray[1];
			},
			data: {
				user: 		name,
				password: password
			}
		});
	}

  function createUser (name, password, properties){
    return $.ajax("/api/createUser", {
      method: "post",
      success: function(data, a, xhr){
        console.log(data)
      },
      data: {
        user: 		name,
        password: password,
        properties: JSON.stringify(properties || {})
      }
    });
  }
  function pullTasks (userId,onSuccessFunc,failureFunc){
	return $.ajax("/api/pullTasks", {
      method: "post",
      success: function(data, a, xhr){
		onSuccessFunc(data);
      },
      data: {
			userId : userId
      }
    });
   
  }
  function pushTask (task,onSuccessFunc,failureFunc){
    return $.ajax("/api/pushTask", {
      method: "post",
      success: function(data, a, xhr){
		onSuccessFunc(data);
      },
	  data: {
			userId : task.userId,
			id : task.id,
			name : task.name
		}
    });
  }
  function deleteTask (task,onSuccessFunc,failureFunc){
    return $.ajax("/api/deleteTask", {
      method: "post",
      success: function(data, a, xhr){
		onSuccessFunc(data);
      },
	  data: {
			id : task.id,
			name : task.name
		}
    });
  }
  function pushSubTask (taskId,subTask,onSuccessFunc,failureFunc){
    return $.ajax("/api/pushSubTask", {
      method: "post",
      success: function(data, a, xhr){
		onSuccessFunc(data);
      },
	  data: {
			taskId : taskId,
			id : subTask.id,
			name : subTask.name,
			isChecked : subTask.isChecked,
		}
    });
  }
  function deleteSubTask (taskId,subTask,onSuccessFunc,failureFunc){
    return $.ajax("/api/deleteSubTask", {
      method: "post",
      success: function(data, a, xhr){
		onSuccessFunc(data);
      },
	  data: {
			taskId : taskId,
			subTaskId : subTask.id,
		}
    });
  }
  function saveScreen (userId,leftCenterWidth,leftCenterHeight,onSuccessFunc,failureFunc){
    return $.ajax("/api/saveScreen", {
      method: "post",
      success: function(data, a, xhr){
		onSuccessFunc(data);
      },
	  data: {
			leftCenterWidth : leftCenterWidth,
			leftCenterHeight : leftCenterHeight,
			userId: userId,
		}
    });
  }
  function pullScreen (userId,onSuccessFunc,failureFunc){
    return $.ajax("/api/pullPreferenceByUserId", {
      method: "post",
      success: function(data, a, xhr){
		onSuccessFunc(data);
      },
	  data: {
			userId: userId,
		}
    });
  }
	return {
		login:      login,
		createUser: createUser,
		pullTasks: pullTasks,
		pushTask: pushTask,
		deleteTask: deleteTask,
		pushSubTask: pushSubTask,
		deleteSubTask: deleteSubTask,
		saveScreen: saveScreen,
		pullScreen: pullScreen
	}
});