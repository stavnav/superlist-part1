/**
 * Created by moshemal.
 */
define(['jquery', 'modules/Login/Login', 'core/cookies', 'core/request',
	'jsFiles/utilities','modules/Data/data','jsFiles/mainLayout','jsFiles/leftPanelLayout','jsFiles/centerPanelLayout','jsFiles/popup'], 
  function($, Login, cookies,request, utilities,data,mainLayout,leftPanelLayout,centerPanelLayout,popup){
  'use strict';
  
  var AUTH_STR = "auth";
  var USER_ID = "userId";

  function startLoggin(){
    function loginSuccess(){
      console.log("login success moving to application");
      startApp();
      login.destroy();
    }

    function loginFail(){
      console.log("login fail tring again");
      login.resetDeferred();
      login.getPromise().then(loginSuccess, loginFail);
    }

    var login = new Login();
    login.appendTo("#container");
    login.getPromise().then(loginSuccess, loginFail);  
  }
  
  function startApp(){	
	window.utilities = utilities;
	window.request = request;
	window.data = data;
	window.mainLayout = mainLayout;
	window.leftPanelLayout = leftPanelLayout;
	window.centerPanelLayout = centerPanelLayout;
	window.popup = popup;
	window.userId = cookies.getCookie(USER_ID);
	data.createData();
	mainLayout.buildSplitterLayout();
  }

  //checking if allready logged in
  if(cookies.getCookie(AUTH_STR) !== "" ) {
    startApp();
  } else {
    startLoggin();
  }
});

